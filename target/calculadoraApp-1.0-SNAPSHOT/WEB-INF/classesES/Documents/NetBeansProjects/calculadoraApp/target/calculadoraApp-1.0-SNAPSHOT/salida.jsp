<%-- 
    Document   : salida
    Created on : 02-04-2020, 12:25:29
    Author     : ANDRES
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculadora Intereses</title>
    </head>
  
    <h1>Resultado del Cálculo</h1>
        
    <%
            
     float interes = (float) request.getAttribute("interes");
     float capital = (float) request.getAttribute("capital_1");
     float tasa = (float) request.getAttribute("tasa_1");
     float ano = (float) request.getAttribute("ano_1");
       
    %>      
        
        <a>Capital:<%=capital%></a>
        <a>Tasa   :<%=tasa%></a>
        <a>Años   :<%=ano%></a>
        <a>Interes:<%=interes%></a>
    
</html>
